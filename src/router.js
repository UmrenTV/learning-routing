import { createRouter, createWebHistory } from 'vue-router';
import TeamsList from './pages/teams/TeamsList.vue';
import UsersList from './pages/users/UsersList.vue';
import TeamMembers from './components/teams/TeamMembers.vue';
import NotFound from './pages/generic/NotFound.vue';
import TeamsFooter from './pages/teams/TeamsFooter.vue';
import UsersFooter from './pages/users/UsersFooter.vue';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', redirect: '/teams' },
    {
      name: 'teams',
      path: '/teams',
      meta: { needsAuth: true },
      // the meta is very useful because you can define a variable like needsAuth here, that gives extra information about the route
      // this can be also used in the navigation guards because we have access to "to" and "from" which have access to this to.meta.needsAuth
      // or from.meta.needsAuth which can help the developers protect a route that requires navigation very easily.
      components: { default: TeamsList, footer: TeamsFooter },
      children: [
        {
          name: 'team-members',
          path: ':teamId',
          component: TeamMembers,
          props: true
        }
      ]
    },
    {
      path: '/users',
      components: { default: UsersList, footer: UsersFooter },
      //just to mention that before enter can be defined in the router config
      // like below, but you can also define them in the components like in
      // UsersList.vue we got with beforeRouteEnter() {} - so same as the
      // created, mounted, lifecycle methods.. you can use the router methods as well.
      beforeEnter(to, from, next) {
        console.log('users beforeEnter');
        console.log(to, from);
        next();
      }
    },
    { path: '/:notFound(.*)', component: NotFound }
  ],
  linkActiveClass: 'active',
  // scrollBehavior(to, from, savedPosition) I am commenting this out
  /* just to show what arguments you can use in the scroll behavior, if
    you want to. I am not using them here but that is why below I replaced
    them with _ and _2. That indicates that I have to take the to and from
    argument in order to reach the third one, but I don't intent on using 
    them. We do this so the IDE doesn't complain by saying "you haven't used
    these. Defined by never used or something"
    The _ and _2 are just argument names that follow a convention, that signal
    to the setup that I need to use these arguments but I don't plan on using them. */
  scrollBehavior(_, _2, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    return { left: 0, top: 0 };
  }
});

//beforeEach runs globaly and it will run on every navigation we request
//sometimes we want that somethimes we dont.
//We can also define beforeEntering specific route like before trying to access
//admin or users, in the route definition above (scroll and see.)
//with the guard beforeEnter like we have in the /users configuration, if we want
//to check for authentication or something like in navigation_guard_ideas.txt
router.beforeEach(function(to, from, next) {
  if (to.meta.needsAuth) {
    console.log('This route requires authentication');
  }
  // since we defined the meta field "needsAuth" up in the configuration for /teams,
  // We can customize the global beforeEach method to check each route before navigating through
  // the meta and customize it to do different things based on the meta.
  // Here we can check if that meta is present, and the user is authetnitcacted, then next(), or else
  // navigate to login or something maybe. So even tho beforeEach is global, we can cutomize it into our advantage for specific cases.
  console.log('Global beforeEach');
  console.log(from, to);
  /* next(); you can use it this way which indicates that you allow
    the navigation, and it will just continue. You can pass false in 
    the method next(false); which will stop the navigation, and most likely
    will show nothing on the screen where router is used because beforeEach
    executes before the router navigates globaly to anything.*/
  // next(false);
  //using next false is not useful here so we changed it, but it has great
  //use if user navigates to a page that requires authentication, and you
  //want to stop it if user is not authenticated.
  //Simple example: if (auth) next() else  next(false)
  // next('/teams') -  you can use a string with a registered route up
  //in the router configuration.
  // next({ name: 'team-members', params: { teamId: 't2' } });
  /* You can also pass an object with full configuration like the example above
    which in this case according to the console creates infinite loop, because even
    if we get there, it will again redirect us there, and call beforeEach again,
    which creates that loop. but as example we should aware of the options that we
     */
  // if we want to use structure simimlar to above, then here is how we can do it
  /* if (to.name === 'team-members') {
      next();
    } else {
      next({ name: 'team-members', params: { teamId: 't2' } });
    } */
  /* Ofcourse this as well doesn't make sense, because unless we go to 
    a team-member link like t1 t2, it will always get us back to team-members/t2
    which makes everything else in our app useless, so we commented it out */
  next();
});

router.afterEach(function(to, from) {
  // sending analytics data
  console.log('global afterEach');
  console.log(to, from);
});

// Generally this "guard" is not used often since when this executes
// the navigation is already completed. This can be handy for storing the navigation
// or sending the users actions to google analytics, or your analytics, since
// it's too late when this cycle happens to control what the user sees on the screen

export default router;
